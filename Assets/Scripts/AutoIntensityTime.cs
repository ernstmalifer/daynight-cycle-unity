﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AutoIntensityTime : MonoBehaviour
{

    public Gradient nightDayColor;

    public Text text;

    public float maxIntensity = 3f;
    public float minIntensity = 0f;
    public float minPoint = -0.2f;

    public float maxAmbient = 1f;
    public float minAmbient = 0f;
    public float minAmbientPoint = -0.2f;


    public Gradient nightDayFogColor;
    public AnimationCurve fogDensityCurve;
    public float fogScale = 1f;

    public float dayAtmosphereThickness = 0.4f;
    public float nightAtmosphereThickness = 0.87f;

    public Vector3 dayRotateSpeed;
    public Vector3 nightRotateSpeed;

    float skySpeed = 1;


    Light mainLight;
    Skybox sky;
    Material skyMat;

    void Start()
    {

        mainLight = GetComponent<Light>();
        skyMat = RenderSettings.skybox;

    }

    void Update()
    {

        
        // transform.Rotate(dayRotateSpeed * Time.deltaTime * skySpeed);

        DateTime time = System.DateTime.Now;
        double hour = time.Hour;
        double minute = (double)time.Minute/60;
        double hourminute = hour + minute;

        double differencebytime = (hourminute % 6)/6 * 90;

        // text.text = time.ToString() + " " + hourminute.ToString();

        // Brute calculate of sun angle by time
        if (hourminute > 0.0 && hourminute <= 6.0) // 12MN to 6AM
        {
            text.text = time.ToString();
            transform.eulerAngles = new Vector3(270 - (int)differencebytime, 0, 0);
        }
        else if (hourminute > 6.0 && hourminute <= 12.0) // 6AM to 12NN
        {
            text.text = time.ToString();
            transform.eulerAngles = new Vector3(180 - (int)differencebytime, 0, 0);
        }
        else if (hourminute > 12.0 && hourminute <= 18.0) // 12NN to 6PM
        {
            text.text = time.ToString();
            transform.eulerAngles = new Vector3(90 - (int)differencebytime, 0, 0);
        }
        else if (hourminute > 18.0 && hourminute < 23.9) // 6PM to 12MN
        {
            text.text = time.ToString();
            transform.eulerAngles = new Vector3(0 - (int)differencebytime, 0, 0);
        }
        else
        {
        }

    }
}